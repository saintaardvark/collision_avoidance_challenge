from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Entry for ESA Collision Avoidance Challenge',
    author='Hugh Brown',
    license='MIT',
)
